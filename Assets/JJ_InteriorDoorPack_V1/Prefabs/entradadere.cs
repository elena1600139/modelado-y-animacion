using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class entradadere : MonoBehaviour
{
    public Animator laPuerta;

    private void OnTriggerEnter(Collider other)
    {
        laPuerta.Play("abrirentdere");
    }

    private void OnTriggerExit(Collider other)
    {
        laPuerta.Play("cerrarentdere");
    }

}
