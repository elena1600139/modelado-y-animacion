using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class entrada : MonoBehaviour
{
    public Animator laPuerta;

    private void OnTriggerEnter(Collider other)
    {
        laPuerta.Play("abrirentrada");
    }

    private void OnTriggerExit(Collider other)
    {
        laPuerta.Play("cerrarentrada");
    }

}
