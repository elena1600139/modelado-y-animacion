using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class abrircerrarnevera : MonoBehaviour
{
    public Animator laPuerta;

    private void OnTriggerEnter(Collider other)
    {
        laPuerta.Play("abrirnev");
    }

    private void OnTriggerExit(Collider other)
    {
        laPuerta.Play("cerrarnev");
    }

}
