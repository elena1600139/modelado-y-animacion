using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class capsulemovement : MonoBehaviour
{
    [SerializeField]
    private Vector3 direction;
    public float speed;

    // Update is called once per frame
    void Update()
    {
        // Acotaci�n de los valores de direcci�n al valor unitario [-1,1]
        direction = ClampVector3(direction);

        // Desplazamiento del componente transform en base al tiempo
        transform.Translate(direction * (speed * Time.deltaTime));
    }
       
    /*
    *Funci�n auxiliar que permite acotar los valores de las componentes
    * de un Vector3 entre - 1 y 1.
    */
    
    public static Vector3 ClampVector3(Vector3 target)
    {
         float clampedx = Mathf.Clamp(target.x, -1f, 1f);
         float clampedy = Mathf.Clamp(target.y, -1f, 1f);
         float clampedz = Mathf.Clamp(target.z, -1f, 1f);
            
         Vector3 result = new Vector3(clampedx, clampedy, clampedz);
            
         return result;
    }
}
